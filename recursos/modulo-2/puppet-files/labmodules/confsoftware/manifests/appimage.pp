class confsoftware::appimage {

	file {'/opt/appimage':
		ensure => directory,
		owner => 'root',
		group => 'root',	
		mode => '0755',
	}

}
