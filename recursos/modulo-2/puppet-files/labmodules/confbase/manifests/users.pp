class confbase::users {

	# Ficheros a manejar con puppet...

	# Ficheros de pam.d

	file {'/etc/passwd':
		owner => 'root',
		group => 'root',
		mode => '0644',
		source => 'puppet:///modules/confbase/users/passwd',
	}

	file {'/etc/shadow':
		owner => 'root',
    group => 'root',
    mode => '0640',
		source => 'puppet:///modules/confbase/users/shadow',
	}

	file {'/etc/group':
		owner => 'root',	
		group => 'root',	
		mode => '0644',
		source => 'puppet:///modules/confbase/users/group',
	}

}
