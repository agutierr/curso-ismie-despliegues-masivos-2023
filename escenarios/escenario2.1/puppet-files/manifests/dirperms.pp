## Clase dirperms que asegura determinados permisos en directorios del sistema
##
 
## Autor: Antonio Gutiérrez Mayoral - Curso ISMIE 2023 
## 

class dirperms {

	# Permisos de /tmp, sticky bit...

	file {'/tmp':
		ensure => 'directory',
		owner => 'root',
		group => 'root',
		mode => '1777',
	}

	# Permisos de /var/tmp, sticky bit...

	file {'/var/tmp':
		ensure => 'directory',
		owner => 'root',
		group => 'root',
		mode => '1777',
	}

	# Permisos en /root/, siempre a 700 y con owner/group a root...
	
	file {'/root':
		ensure => 'directory',
		owner => 'root',
		group => 'root',
		mode => '0700',
	}

}
