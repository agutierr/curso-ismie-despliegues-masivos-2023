#!/bin/bash

     
# Generamos certificados para ldap-server & tls
mkdir /etc/ssl/certs/ldap-server.iessonrisasylagrimas.cmadrid.org/
cd /etc/ssl/certs/ldap-server.iessonrisasylagrimas.cmadrid.org/
## Generamos CA key y certificado
openssl genrsa -out rootCA.key 2048
openssl req -x509 -new -nodes -subj "/CN=ldap-server.iessonrisasylagrimas.cmadrid.org" -key rootCA.key -sha256 -days 1024 -out rootCA.pem
## Generamos CSR
openssl genrsa -out ldap-server.iessonrisasylagrimas.cmadrid.org_privatekey.pem 2048
openssl req -new -subj "/CN=ldap-server.iessonrisasylagrimas.cmadrid.org" -key ldap-server.iessonrisasylagrimas.cmadrid.org_privatekey.pem -out ldap-server.iessonrisasylagrimas.cmadrid.org_privatekey.csr
openssl x509 -req -in ldap-server.iessonrisasylagrimas.cmadrid.org_privatekey.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out ldap-server.iessonrisasylagrimas.cmadrid.org.pem -days 3650 -sha256
pwd
ls
