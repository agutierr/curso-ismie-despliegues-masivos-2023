#!/bin/bash

apt-get update
apt-get install -y vim htop openssl screen tmux debconf-utils pwgen
# Ajustar hostname
tmpfile=`mktemp`
cat /etc/hosts | grep -v ldap-server > $tmpfile
echo "192.168.56.5  ldap-server.iessonrisasylagrimas.cmadrid.org ldap-server" >> $tmpfile
mv $tmpfile /etc/hosts
echo "set tabstop=2" >> /root/.vimrc
echo "set mouse-=a" >> /root/.vimrc

exit 0
