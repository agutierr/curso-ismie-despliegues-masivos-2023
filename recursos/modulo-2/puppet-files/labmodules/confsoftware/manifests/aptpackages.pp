class confsoftware::aptpackages {

	## PLANTILLA
	## package {'nombre-paquete-apt':
  ##   ensure => latest,
  ##   provider => apt,
  ## }

	# Utilidad screen.

	package {'screen':
		ensure => latest,
		provider => apt,
	}

	# Utilidad tmux

	package {'tmux':
		ensure => latest,
		provider => apt,
	}

	package {['vim', 'nano', 'joe']:
		ensure => latest,
		provider => apt,
	}

}
