## Clase software, aquí vamos a definir todo lo relacionado con el software que tiene
## que estar instalado en nuestros ordenadores.
##

## Autor: Antonio Gutiérrez Mayoral - Curso ISMIE 2023
##

class software {

	# Todos los paquetes que tienen que ser instalados vía apt-get aquí... 

	package {'screen':
		ensure => latest,
		provider => 'apt',
	}

	package{'tmux':
		ensure => latest,
		provider => 'apt',
	}

	# Paquetes que provegan de pip3...
	
	package {'python-pip':
		ensure => latest,
		provider => 'apt',
	}

	package {'django':
		ensure => latest,
		provider => 'pip',
		require => Package['python-pip'],
	}
	

}
