#!/bin/bash

VBOXMANAGE=/usr/bin/VBoxManage
VAGRANT=/usr/bin/vagrant

#MIRROR="mirror.eif.urjc.es"
MIRROR=""
REMOTE_DIR=/var/www/html/ovas/

### VARIABLES DE EXPORTACIÓN DE OVA.
VMNAME="ENTORNO-LAMP"
PRODUCT="ENTORNO LAMP"
PRODUCTURL="https://iessonrisasylagrimas.cmadrid.org"
VENDOR="IES Sonrisas Y Lagrimas"
VENDORURL="https://iessonrisasylagrimas.cmadrid.org"
VERSION="0.1-`date +%F`"
DESCRIPTION="Imagen exportada IES Sonrisas y Lágrimas"

WD=$PWD

OVA_NAME="ENTORNO-LAMP"

# borramos ovas antiguas
# rm ovas/*

tmpdir=`mktemp -d`

mkdir -p $tmpdir/ovas/

if ! [ -d $1 ];
then
	echo "Uso: ./vagrant-to-ova <vagrant-work-dir>"
	exit 1
fi

if ! [ -f $1/Vagrantfile  ];
then
	echo "No existe Vagrantfile en $1 :-("
	exit 2
fi

cd $1

# cp -arv data/ $tmpdir
cp Vagrantfile $tmpdir

cd $tmpdir 

# Actualizamos la box si necesario
vagrant box update

# Arrancamos la máquina virtual
vagrant up

if [ $? -eq 0 ]; then
	VM_UID=`cat $tmpdir/.vagrant/machines/default/virtualbox/id`
	VM_NAME=`VBoxManage list vms | grep $VM_UID | awk '{print $1}' | sed s/'"'/''/g`

	echo "Apagando la máquina: sudo shutdown -h now"
	echo ""

	$VAGRANT ssh --command "sudo usermod -L vagrant && sudo shutdown -h now"
		
	sleep 10	

	echo "Exportando máquina virtual $VM_NAME con UID $VM_UID"
	echo ""

	$VBOXMANAGE export "$VM_NAME" --output $WD/$OVA_NAME-`date +%d%m%Y`.ova \
		--ovf20 \
		--manifest \
		--vsys 0 \
		--vmname "$VMNAME" \
	  --product "$PRODUCT" \
    --producturl "$PRODUCTURL" \
		--vendor "$VENDOR" \
    --vendorurl "$VENDORURL" \
		--version "$VERSION" \
		--description "$DESCRIPTION"

	echo "Destruyendo la máquina..."
	echo ""

	$VAGRANT destroy -f

	# Borramos OVAs antiguas, por espacio
	# echo "Borrando OVAs antiguas en $MIRROR/$REMOTE_DIR..."
	# ssh -o StrictHostKeyChecking=no -i scripts/.ssh/id_rsa-ovas root@$MIRROR "rm $REMOTE_DIR/*"

	## Subir imagen a mirror si está definida variable MIRROR
	## Pasar ssh/scp con -i si tenemos clave publica... -i scripts/.ssh/id_rsa-ovas
	if [ "$MIRROR" != "" ]; 
	then
		echo "Publicando OVA a $MIRROR..."
		echo ""

		scp -o StrictHostKeyChecking=no $WD/$OVA_NAME-`date +%d%m%Y`.ova root@$MIRROR:$REMOTE_DIR/
		ssh -o StrictHostKeyChecking=no root@$MIRROR "chmod -R 755 $REMOTE_DIR"
	fi

  rm -rf $tmpdir

	echo "¡¡¡ ÉXITO !!!"

	exit 0
else
	echo "No se pudo levantar la máquina :-("
	exit 1
fi

echo "Fichero resultante exportado con éxito: " $WD/$OVA_NAME-`date +%d%m%Y`.ova
echo ""

cd $WD
