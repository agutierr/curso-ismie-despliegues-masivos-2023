#!/bin/bash

tmpfile1=`mktemp`

# Asignamos permisos de lectura al grupo openldap para los certificados...

chgrp openldap /etc/ssl/certs/ldap-server.iessonrisasylagrimas.cmadrid.org/*
chmod g+r /etc/ssl/certs/ldap-server.iessonrisasylagrimas.cmadrid.org/*

echo "dn: cn=config" > $tmpfile1
echo "changetype: modify" >> $tmpfile1
echo "replace: olcTLSCertificateFile" >> $tmpfile1
echo "olcTLSCertificateFile: /etc/ssl/certs/ldap-server.iessonrisasylagrimas.cmadrid.org/ldap-server.iessonrisasylagrimas.cmadrid.org.pem" >> $tmpfile1
echo "-" >> $tmpfile1
echo "replace: olcTLSCertificateKeyFile" >> $tmpfile1
echo "olcTLSCertificateKeyFile: /etc/ssl/certs/ldap-server.iessonrisasylagrimas.cmadrid.org/ldap-server.iessonrisasylagrimas.cmadrid.org_privatekey.pem" >> $tmpfile1

ldapmodify -Y EXTERNAL -H ldapi:// -f $tmpfile1 

# Añadimos PEM de la CA

tmpfile2=`mktemp`

echo "dn: cn=config" > $tmpfile2
echo "changetype: modify" >> $tmpfile2
echo "add: olcTLSCACertificateFile" >> $tmpfile2
echo "olcTLSCACertificateFile: /etc/ssl/certs/ldap-server.iessonrisasylagrimas.cmadrid.org/rootCA.pem" >> $tmpfile2

ldapmodify -Y EXTERNAL -H ldapi:// -f $tmpfile2

rm $tmpfile1
rm $tmpfile2

echo "Reiniciamos slapd despues de aplicar certificados TLS..."
