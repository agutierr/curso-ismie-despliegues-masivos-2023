## Clase users, aquí vamos a definir todo lo relacionado con los usuarios de nuestro
## sistema. 

## Autor: Antonio Gutiérrez Mayoral - Curso ISMIE 2023 
## 

class users {

	group {'alumno':
		ensure => present,
		gid => '1100'
	}

	user {'alumno':
		ensure => present,
		comment => 'Usuario alumno',
		uid => '1100',
		gid => '1100',
		require => Group['alumno'],
		home => '/home/alumnos/alumno',
		managehome => 'true',
		shell => '/bin/bash',
		password => '$y$j9T$9i1maB9b6OAJq8Lb0jmJE.$BvR8ro2.F0pN1H6H2U3UtyD7Sp4rOY1UySHSNNqVzs9',
	}

}
