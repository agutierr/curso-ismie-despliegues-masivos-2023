#!/bin/bash

# instalacion paquetes básicos
echo "Instalando paquetes básicos..."
apt-get update 1> /dev/null
apt-get install -y vim screen tmux htop pwgen httpie 1> /dev/null

# actualizacion
echo "Actualización de paquetes de la imagen..." 
apt-get upgrade -y 1> /dev/null

# pre-ajustes instalacion docker
echo "Pre-ajuste de instalación de docker..."
apt-get remove -y docker docker-engine docker.io containerd runc 1> /dev/null
apt-get -y install ca-certificates curl gnupg 1> /dev/null 
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg 1> /dev/null
chmod a+r /usr/share/keyrings/docker-archive-keyring.gpg 1> /dev/null
echo "deb [arch="$(dpkg --print-architecture)" signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | tee /etc/apt/sources.list.d/docker.list 1> /dev/null

echo "Instalación de docker..."
apt-get update 1> /dev/null
apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin 1> /dev/null 

# add vagrant user to docker group 
echo "Añadimos usuario vagrant a grupo docker..."
addgroup vagrant docker 1> /dev/null 

# create volume portainer_data (needed for running portainer...)
echo "Instalación de portainer..." 
docker volume create portainer_data 1> /dev/null 

# run portainer agent (needed to connect throught portainer api to local docker daemon...)
docker run -d -p 9001:9001 --name portainer_agent --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/docker/volumes:/var/lib/docker/volumes portainer/agent:2.18.2 1> /dev/null 2> /dev/null
# run portainer
docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest 1> /dev/null 2> /dev/null
 
# change portainer admin password using API
portainer_pw=`pwgen -s -n 12 1 | sed s/'\n'/''/g ` 
echo "Fijamos contraseña de admin en portainer a través del API..."

http --ignore-stdin --verify no POST https://localhost:9443/api/users/admin/init Username="admin" Password="$portainer_pw" 1> /dev/null 

echo ""
echo "¡LISTO! Conéctate al interfaz web de Portainer en la siguiente URL: https://localhost:9443/"
echo "El usuario es 'admin' y la contraseña es '$portainer_pw'"
echo ""
echo " Enjoy!"
echo ""

exit 0
