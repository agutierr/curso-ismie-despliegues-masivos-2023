## Clase sshauthkey, esta clase lo único que hace es poner en los clientes, la clave pública
## de una clave ssh que hemos generado previamente en otro lugar (ie servidor puppet)
##

## Autor: Antonio Gutiérrez Mayoral - Curso ISMIE 2023
##

class sshauthkey {

	# Aqui controlamos el fichero /root/.ssh/authorized_keys

	file {'/root/.ssh/authorized_keys':
		ensure => present,
		owner => 'root',
		group => 'root',	
		mode => '0644',
		content => 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC5Eiv8bMvmYO4LBT1STNEb1+51usWOeS+nL+DFKnbMdotzTE6zkIFbWxLKY2DiFfF3h3U5+i2gP+u3rBH9YAIpdweBWLXpVFt4au0Hh2yNgnVen/YsNoJ5X3xlKre6d+/sPPB7ej1WOf5nANT0kIqVk8iF2B9eYxe+7/1ha9b8vvnwzvbn78Ps2eiaGJcOZnQBbGkTts9/YaVdBj13shB9K3MFQiapd5rsAaEcjVeTSd6UkV+5TTvc8lp52FQv6LTRWnHgszGeuzLIDuuK1oSouAPvpsJXrygHDtJtugtUQmLk7WSeasNkX7J+isKjrw+4LTSbDn49+o7o35nmBy7mCuTCQiFV/dWraEiqMfaVapFp3i/+Lm5J7ftd0rsSou4EARR1XTp3IhUfzJ2s6LP8wVtI8BGJQpFjSGADO9+dN0cCVATGpiyOGozMop2gfrJPuiZOtx96cQ68QxKzByG9H+c+XoIR86t8GeG1PTCL91RUMNaCr9AVX3R58la1tPk= root@puppet',
	}

}
