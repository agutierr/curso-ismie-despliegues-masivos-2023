#!/bin/bash

touch /etc/ldap/ldap.conf

echo "base dc=iessonrisasylagrimas,dc=cmadrid,dc=org" > /etc/ldap/ldap.conf
echo "uri ldap://ldap-server.iessonrisasylagrimas.cmadrid.org" >> /etc/ldap/ldap.conf
echo "" >> /etc/ldap/ldap.conf
echo "ssl start_tls" >> /etc/ldap/ldap.conf
echo "tls_reqcert never" >> /etc/ldap/ldap.conf

ln -s /etc/ldap/ldap.conf /etc/ldap.conf

service slapd restart
