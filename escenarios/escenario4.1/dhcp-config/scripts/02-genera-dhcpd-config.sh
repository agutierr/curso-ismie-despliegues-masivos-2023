#!/bin/bash

### Genera los ficheros de configuración de DHCP en base a ficheros
### de texto con la info de macs-ip-hostname, etc.

### PLANTILLA DE SALIDA PARA EL FICHERO DE CONFIGURACION DE DHCP.
# #Workstation: aula001-pc01.iessonrisasylagrimas.cmadrid.org
# #IP: 192.168.56.65
# #MAC Address: 10:e7:c6:17:9f:b9
# host aula001-pc01.iessonrisasylagrimas.cmadrid.org {
#  hardware ethernet 10:e7:c6:17:9f:b9;
#  fixed-address 192.168.56.65;
#  option routers 192.168.56.1;
#  filename "/boot/bios/aula-001/pxelinux.0";
#  next-server boot.iessonrisasylagrimas.cmadrid.org;
# }
#
#
###

if [ "$#" != 1 ]; then
  echo "Uso de este script: ./$0 <conf-dir>"
  echo ""
  exit 1
fi

# Comprobamos que existe directorio $1

if ! [ -d $1 ]; then
  echo "$1 no es un directorio. Ejecute ./$0 <conf-dir>"
  echo ""
  exit 2
else
  # Creamos un fichero temporal para escribir ahí el resultado
  tmpfile=$(mktemp)
  mkdir artifacts/
  >artifacts/hosts
  echo "## Generado: $(date +%F-%T)" >>artifacts/hosts
  echo "## NO MODIFIQUE ESTE FICHERO A MANO!!! Se ha generado automágicamente con Git-ops :-)" >>artifacts/hosts
  echo "" >>artifacts/hosts

  for file in $(find $1 -mindepth 1 -maxdepth 1 -name aula\*\.conf\.txt -type f); do
    filename=/tmp/$(basename $file | cut -d'.' -f1).conf
    # Creamos vacio el fichero filename
    >$filename
    echo "## Aula: $file" >>$filename
    echo "## Generado: $(date +%F-%T)" >>$filename
    echo "## NO MODIFIQUE ESTE FICHERO A MANO!!! Se ha generado automágicamente con Git-ops :-)" >>$filename
    echo "" >>$filename

    filename=/tmp/$(basename $file | cut -d'.' -f1).conf
    while read line; do
      # Saltamos comentarios...
      if [[ "$line" = \#* ]]; then continue; fi
      # Leeemos todos los campos y cortamos con cut -d' ' -fPOSICION
      hostname=$(echo $line | cut -d' ' -f1)
      dominio=$(echo $line | cut -d' ' -f2)
      ip=$(echo $line | cut -d' ' -f3)
      mac=$(echo $line | cut -d' ' -f4)
      pxefile=$(echo $line | cut -d' ' -f5)
      nextserver=$(echo $line | cut -d' ' -f6)
      echo "host $hostname.$dominio {" >>$filename
      echo " hardware ethernet $mac;" >>$filename
      echo " fixed-address $ip;" >>$filename
      echo " option routers 192.168.56.1;" >>$filename
      echo " filename \"$pxefile\";" >>$filename
      echo " next-server $nextserver;" >>$filename
      echo "}" >>$filename
      echo "" >>$filename
      # Generamos hosts file. (/etc/hosts)
      echo $ip" "$hostname.$dominio" "$hostname >>artifacts/hosts
    done <$file
    echo "Copiando fichero $filename a /etc/dhcp/dhcp.conf.d/ ..."
    sudo cp $filename /etc/dhcp/dhcp.conf.d/$(basename $file | cut -d'.' -f1).conf
    # Borramos fichero temporal
    rm $filename
  done
fi

exit 0
