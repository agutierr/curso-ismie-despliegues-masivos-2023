#!/bin/bash

### Comprueba que la sintaxis de los campos de los ficheros de configuración es correcta.
###

if [ "$#" != 1 ]; then
  echo "Uso de este script: ./$0 <conf-dir>"
  echo ""
  exit 1
fi

error=0
linea=1

# Comprobamos que existe directorio $1

if ! [ -d $1 ]; then
  echo "$1 no es un directorio. Ejecute ./$0 <conf-dir>"
  echo ""
  exit 2
else
  for file in $(find $1 -mindepth 1 -maxdepth 1 -name aula\*\.conf\.txt -type f); do
    echo "Comprobando fichero: $file"
    linea=1
    while read line; do
      # Saltamos comentarios...
      if [[ "$line" = \#* ]]; then
        linea=$(echo $linea+1 | bc)
        continue
      fi
      host=$(echo $line | cut -d' ' -f1)
      dnsdomain=$(echo $line | cut -d' ' -f2)
      ip=$(echo $line | cut -d' ' -f3)
      mac=$(echo $line | cut -d' ' -f4)
      pxefile=$(echo $line | cut -d' ' -f5)
      pxeserver=$(echo $line | cut -d' ' -f6)
      if ! [[ $host =~ ^aula([0-9]{3}-pc[0-9][0-9])$ ]]; then
        error=1
        echo "Error en el campo host, en fichero $file, linea: $linea :-("
        break
      fi
      if ! [[ $dnsdomain =~ ^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*$ ]]; then
        error=1
        echo "Error en el campo dnsdomain, en fichero $file, linea $linea :-("
        break
      fi
      if ! [[ $ip =~ ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$ ]]; then
        error=1
        echo "Error en el campo sufijo IP, en fichero $file :-("
        break
      fi
      if ! [[ $mac =~ ^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$ ]]; then
        error=1
        echo "Error en el campo sufijo MAC, en fichero $file :-("
        break
      fi
      if ! [[ $pxefile =~ ^(.+)\/([^\/]+)$ ]]; then
        error=1
        echo "Error en el campo sufijo PXEFILE, en fichero $file :-("
        break
      fi
      if ! [[ $pxeserver =~ ^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*$ ]]; then
        error=1
        echo "Error en el campo sufijo PXESERVER, en fichero $file :-("
        break
      fi
      linea=$(echo $linea+1 | bc)
    done <$file
    if [ "$error" == "1" ]; then
      echo "Se produjo error en $file, saliendo..."
      break
    fi
  done
fi

if [ "$error" == "0" ]; then
  echo "Exito!"
fi

exit $error
