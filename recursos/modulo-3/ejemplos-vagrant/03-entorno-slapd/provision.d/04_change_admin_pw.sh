#!/bin/bash

tmpfile1=`mktemp`
tmpfile2=`mktemp`

pw1=`pwgen -y -s -n 16 1`
pw2=`slappasswd -s "$pw1"`

echo "dn: cn=admin,dc=iessonrisasylagrimas,dc=cmadrid,dc=org" > $tmpfile1
echo "changetype: modify" >> $tmpfile1
echo "replace: userPassword" >> $tmpfile1
echo "userPassword: $pw2" >> $tmpfile1

echo "dn: olcDatabase={1}mdb,cn=config" > $tmpfile2
echo "changetype: modify" >> $tmpfile2
echo "replace: olcRootPW" >> $tmpfile2
echo "olcRootPW: $pw2" >> $tmpfile2

# Aplicamos cambios con ldapmodify
ldapmodify -H ldapi:/// -x -D cn=admin,dc=iessonrisasylagrimas,dc=cmadrid,dc=org -w admin -f $tmpfile1
ldapmodify -H ldapi:/// -Y EXTERNAL -f $tmpfile2

echo ""
echo " Instalación de slapd completada. Se muestran las credenciales a continuación:"
echo "  Administrador: cn=admin,dc=iessonrisasylagrimas,dc=cmadrid,dc=org"
echo "  Contraseña:    $pw1"
echo ""
echo " Tenga en cuenta que esto es un servidor de pruebas para fines educativos, no debe"
echo " ser usado en producción!!!"
echo ""
