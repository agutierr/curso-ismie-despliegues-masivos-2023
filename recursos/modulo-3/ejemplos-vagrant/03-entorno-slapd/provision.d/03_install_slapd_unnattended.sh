#!/bin/bash

echo "slapd slapd/password1 password admin" | debconf-set-selections 
echo "slapd slapd/internal/adminpw password admin" | debconf-set-selections
echo "slapd slapd/internal/generated_adminpw password admin" | debconf-set-selections
echo "slapd slapd/password2 password admin" | debconf-set-selections 
echo "slapd slapd/domain string iessonrisasylagrimas.cmadrid.org" | debconf-set-selections

export DEBIAN_FRONTEND=noninteractive

apt install ldap-utils slapd -y

exit 0
