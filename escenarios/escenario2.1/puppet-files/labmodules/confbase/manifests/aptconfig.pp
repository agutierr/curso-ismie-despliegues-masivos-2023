class confbase::aptconfig {

	include apt

	apt::source { "es.archive.ubuntu.com-${lsbdistcodename}":
    location => 'http://es.archive.ubuntu.com/ubuntu/',
    repos    => 'main universe multiverse restricted',
    architecture => 'i386,amd64',
  } ->

  apt::source { "es.archive.ubuntu.com-${lsbdistcodename}-security":
    location => 'http://es.archive.ubuntu.com/ubuntu/',
    repos    => 'main universe multiverse restricted',
    architecture => 'i386,amd64',
    release  => "${lsbdistcodename}-security"
  } ->

  apt::source { "es.archive.ubuntu.com-${lsbdistcodename}-updates":
    location => 'http://es.archive.ubuntu.com/ubuntu/',
    repos    => 'main universe multiverse restricted',
    architecture => 'i386,amd64',
    release  => "${lsbdistcodename}-updates"
  } ->

  apt::source { "es.archive.ubuntu.com-${lsbdistcodename}-backports":
    location => 'http://es.archive.ubuntu.com/ubuntu/',
    repos    => 'main universe multiverse restricted',
    architecture => 'i386,amd64',
    release  => "${lsbdistcodename}-backports"
  } 

}
